jQuery(document).ready(function($){

	function scrollDown() {
		var vheight = $(window).height();
		$('html, body').animate({
		  scrollTop: (Math.floor($(window).scrollTop() / vheight)+1) * vheight
		}, 500);  
	};

	$('.scroll-down').click(function(event){
		scrollDown();
		event.preventDefault();
	});

	$('.nav-button').click(function(){
		$('body').toggleClass('nav-open');
	});
});

